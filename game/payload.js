// avoid activating arcade mode
Runner.instance_.setArcadeMode = function () { }
// allow game to play in the background
Runner.instance_.onVisibilityChange = function () { }

var clamp = function (x, y) {
    if (typeof y === "undefined") {
        y = 1000;
    }
    return x > y ? y : x;
}

// observer
Controller = (function () {
    var LOOP_INTERVAL_MS = 50;
    var observerLoop = undefined;
    var ws = undefined;

    var sendKeyEvent = function (key, eventtype) {
        var event = document.createEvent('Event');
        event.initEvent(eventtype, true, true);
        event.keyCode = key;

        document.dispatchEvent(event);
    }

    var stop = function () {
        clearInterval(observerLoop);
        observerLoop = undefined;
        ws.close();
    };

    var play = function () {
        if (!observerLoop) {
            observerLoop = setInterval(function () {
                // this is the sensor
                var obstacles = Runner.instance_.horizon.obstacles;
                var trex = Runner.instance_.tRex;

                var closest = obstacles.find(function (obstacle) {
                    return obstacle.xPos - trex.xPos > 0;
                });
                
                var closestData = {};
                if (closest) {
                    closestData = {
                        type: closest.typeConfig.type,
                        distance: clamp(closest.xPos - trex.xPos) / 10,
                        height: clamp(closest.yPos, 200) / 2,
                        width: clamp(closest.size * closest.typeConfig.width, 100),
                        speed: clamp(Runner.instance_.currentSpeed + closest.speedOffset, 100)
                    };
                }

                const data = {
                    closest: closestData,
                    flags: {
                        playing: Runner.instance_.playing,
                        crashed: Runner.instance_.crashed,
                        paused: Runner.instance_.paused
                    },
                    fitnessMetadata: {
                        ...closestData,
                        distanceRan: Runner.instance_.distanceRan
                    },
                    playCount: Runner.instance_.playCount
                };
                ws.send(JSON.stringify(data));
            }, LOOP_INTERVAL_MS);
        }
    };

    var pause = function () {
        if (observerLoop) {
            clearInterval(observerLoop);
            observerLoop = undefined;
        }
    }

    var start = function () {
        ws = new WebSocket("ws://localhost:8081");

        ws.onmessage = function (event) {
            const data = JSON.parse(event.data);

            switch (data.type) {
                case "start":
                    // press and release space
                    sendKeyEvent(32, "keydown");
                    sendKeyEvent(32, "keyup");
                    break;
                case "togglepause":
                    if (Runner.instance_.paused) {
                        play();
                        Runner.instance_.tRex.reset();
                        Runner.instance_.play();
                    } else {
                        pause();
                        Runner.instance_.stop();
                    }
                    break;
                case "keydown":
                    sendKeyEvent(data.key, "keydown");
                    break;
                case "keyup":
                    sendKeyEvent(data.key, "keyup");
                    break;
                case "restart":
                    Runner.instance_.restart()
                    break;
            };
        };

        ws.onclose = function () {
            Controller.stop();
        }

        play();
    };

    return {
        start: start,
        stop: stop,
        play: play,
        pause: pause
    };
})();

Controller.start();