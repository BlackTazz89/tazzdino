const synaptic = require('synaptic');

class Genome {
    constructor(inputs, hiddenLayers, outputs, mutationRate) {
        this.inner = new synaptic.Architect.Perceptron(inputs, ...hiddenLayers, outputs);
        this.mutationRate = mutationRate;
        this.fitness = 0;
        this.generation = 0;
    }

    activate(inputs) {
        return this.inner.activate(inputs);
    }

    serialize() {
        return {
            inner: this.inner.toJSON(),
            mutationRate: this.mutationRate,
            fitness: this.fitness,
            generation: this.generation
        }
    }

    static fromJSON({ inner, mutationRate, fitness, generation }) {
        const genome = Object.create(Genome.prototype);
        genome.inner = synaptic.Network.fromJSON(inner);
        genome.mutationRate = mutationRate;
        genome.fitness = fitness;
        genome.generation = generation;
        return genome;
    }
}

module.exports = Genome;