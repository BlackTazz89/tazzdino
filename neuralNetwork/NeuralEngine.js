class NeuralEngine {
    constructor(genomes, newGenerationStrategy, logger) {
        this.genomes = genomes;
        this.newGenerationStrategy = newGenerationStrategy;
        this.logger = logger;
        this.status = { currentIdx: null, currentGenome: null, total: genomes.length };
        this.running = false;
    }

    async start() {
        this.running = true;
        for (const [index, genome] of this._genomeGenerator()) {
            this.status.currentIdx = index;
            this.status.currentGenome = genome;
            var fitness = 0;
            while ((fitness = await new Promise(resolve => this._onInput = resolve)) !== null) {
                genome.fitness = fitness;
            }
            this.logger.log(`Genome ${index + 1} of gen ${genome.generation} died with fitness \u2192 ${genome.fitness.toFixed(3)}`);
        }
        this.running = false;
    }

    *_genomeGenerator() {
        while (this.genomes = this._newGeneration()) {
            yield* this.genomes.entries();
        }
    }

    _newGeneration() {
        const newGeneration = this.newGenerationStrategy(this.genomes);
        newGeneration.forEach((genome) => {
            genome.fitness = 0;
            genome.generation += 1
        });
        return newGeneration;
    }

    updateFitness(fitness) {
        if (!this.running) {
            throw "Engine is not running"
        }
        this._onInput(fitness);
    }

    inputs(inputs) {
        if (!this.running) {
            throw "Engine is not running"
        }
        return this.status.currentGenome.activate(inputs);
    }

    getStatus() {
        return {
            ...this.status,
            genomes: this.genomes
        }
    }

    end() {
        if (!this.running) {
            throw "Engine is not running"
        }
        this._onInput(null);
    }
}

module.exports = NeuralEngine;