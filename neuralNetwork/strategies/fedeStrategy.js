const synaptic = require('synaptic');
const _ = require('lodash');

function crossOver(genomeA, genomeB) {
    const A = _.cloneDeep(genomeA).inner.toJSON();
    const B = _.cloneDeep(genomeB).inner.toJSON();

    const crossPoint = _.random(0, A.connections.length);
    for (let i = 0; i < crossPoint; i++) {
        A.connections[i] = _.cloneDeep(B.connections[i]);
    }

    const ret = _.cloneDeep(genomeA);
    ret.inner = synaptic.Network.fromJSON(A);
    return ret;
}

// https://stackoverflow.com/questions/25582882/javascript-math-random-normal-distribution-gaussian-bell-curve
function gaussian_random() {
    var u = 0, v = 0;
    while (u === 0) u = Math.random(); //Converting [0,1) to (0,1)
    while (v === 0) v = Math.random();
    return Math.sqrt(-2.0 * Math.log(u)) * Math.cos(2.0 * Math.PI * v);
}

function mutate(genomeA) {
    const A = genomeA.inner.toJSON();
    A.connections.forEach(connection => {
        if (Math.random() < genomeA.mutationRate) {
            connection.weight += gaussian_random();
        }
    });
    A.neurons.forEach(neuron => {
        if (Math.random() < genomeA.mutationRate) {
            neuron.bias += gaussian_random();
        }
    });
    const ret = _.cloneDeep(genomeA);
    ret.inner = synaptic.Network.fromJSON(A);
    return ret;
}

function newGeneration(genomes) {
    const samples = [];
    genomes.forEach(function (genome) {
        for (let i = 0; i < 1 + genome.fitness; i++) {
            samples.push(genome);
        }
    });
    const newGeneration = [];
    for (let i = 0; i < genomes.length; i++) {
        const [mother, father] = _.sampleSize(samples, 2);
        newGeneration.push(mutate(crossOver(mother, father)));
    }
    return newGeneration;
}

module.exports = newGeneration;