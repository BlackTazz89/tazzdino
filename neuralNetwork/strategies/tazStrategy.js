const synaptic = require('synaptic');
const _ = require('lodash');

function crossOver(genomeA, genomeB) {
    const first = _.cloneDeep(genomeA);
    const second = _.cloneDeep(genomeB);

    const firstJsonGenome = first.inner.toJSON();
    const secondJsonGenome = second.inner.toJSON();

    const crossOverPoint = Math.round(firstJsonGenome.neurons.length * Math.random());
    for (let i = crossOverPoint; i < firstJsonGenome.neurons.length; ++i) {
        [firstJsonGenome.neurons[i]['bias'], secondJsonGenome.neurons[i]['bias']] = [secondJsonGenome.neurons[i]['bias'], firstJsonGenome.neurons[i]['bias']]
    }
    //choosing randomly crossover genome
    const choosenGenome = Math.random() > 0.5 ? firstJsonGenome : secondJsonGenome;
    first.inner = synaptic.Network.fromJSON(choosenGenome);
    return first;
}

function mutate(genome) {
    const genomeJson = genome.inner.toJSON();
    for (let keys of [['neurons', 'bias'], ['connections', 'weight']]) {
        const [key, attribute] = keys;
        for (let i = 0; i < genomeJson[key].length; i++) {
            if (Math.random() <= genome.mutationRate) {
                genomeJson[key][i][attribute] += genomeJson[key][i][attribute] * (Math.random() - 0.5) * 3 + (Math.random() - 0.5) * 5;
            }
        }
    }
    genome.inner = synaptic.Network.fromJSON(genomeJson);
    return genome;
}

function newGeneration(genomes) {
    const bestFitnessGenomes = _.sortBy(genomes, 'fitness').reverse().slice(0, Math.max(1, Math.round(genomes.length * 0.4)));
    const [bestOne, ...rest] = bestFitnessGenomes;
    const newGeneration = bestOne.fitness > 0 ? [bestOne] : [];
    for (let i = newGeneration.length; i < genomes.length; i++) {
        newGeneration.push(mutate(crossOver(_.sample(bestFitnessGenomes), _.sample(bestFitnessGenomes))));
    }
    return newGeneration;
}

module.exports = newGeneration;