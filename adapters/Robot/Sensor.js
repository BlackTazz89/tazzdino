class Sensor {
    constructor([sensorX, sensorY, sensorWidth, sensorHeight], spriteLocator, logger, connector) {
        this.sensorX = sensorX;
        this.sensorY = sensorY;
        this.sensorWidth = sensorWidth;
        this.sensorHeight = sensorHeight;
        this.spriteLocator = spriteLocator;
        this.logger = logger;
        this.connector = connector;
        this.size = 0;
        this._rawSize = 0;
        this.distance = 100;
        this.speed = 0;
        this.height = 0;
        this.measuresPerObstacle = 0;
        this.paused = false;
    }

    activate(interval) {
        this.logger.log('Activating sensor');
        let lastMisureDate = Date.now();

        let currentInput, previousInput, currentOutput, previousOutput;

        this.readIntervalId = setInterval(() => {
            if (!this.paused) {
                let { distance, size, height } = this.spriteLocator.getNextObstacleDetails([this.sensorX, this.sensorY, this.sensorWidth, this.sensorHeight]);
                const now = Date.now();
                const deltaDistance = this.distance - distance;
                if (deltaDistance > 0) {
                    ++this.measuresPerObstacle;
                    this.speed += (this.distance - distance) / (now - lastMisureDate);
                    this.distance = distance;
                    this.size = Math.max(this.size, size);
                    this.height = Math.max(this.height, height);
                } else if (deltaDistance < 0 && distance > (this._rawSize + this.distance)) {
                    this.distance = distance;
                    this.size = size;
                    this.height = height;
                    this.speed = 0;
                    this.measuresPerObstacle = 0;
                } else if (deltaDistance < 0) {
                    this.distance = 0;
                }
                this._rawSize = size;
                lastMisureDate = now;
                this.connector.notifyObservers({
                    flags: {
                        crashed: false,
                        playing: true,
                        paused: this.paused
                    },
                    closest: {
                        distance: this.distance,
                        width: this.size,
                        height: this.height,
                        speed: (this.measuresPerObstacle ? this.speed / this.measuresPerObstacle : 0)*100,
                        type: '',
                    },
                    fitnessMetadata: {
                        distance: this.distance,
                        width: this.size,
                        height: this.height,
                        speed: this.speed*100
                    }
                });
            }
        }, interval);
    }

    pause() {
        this.paused = !this.paused;
    }

    stop() {
        clearInterval(this.readIntervalId);
        this.logger.log('Sensor stop');
        this.reset();
    }

    reset() {
        this.logger.log('Sensor reset');
        this.size = 0;
        this._rawSize = 0;
        this.distance = 100;
        this.speed = 0;
        this.height = 0;
        this.measuresPerObstacle = 0;
    }
}

module.exports = Sensor;