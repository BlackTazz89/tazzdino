const robot = require('robotjs');

class SpriteLocator {
    constructor() {
        this.screenSize = robot.getScreenSize();
        this.dinoColorv1 = '535353';
        this.dinoColorv2 = '545454'; //WTF Dino's color change based on resolution o_O
        this.defaultBg = 'f7f7f7';
        this.gameoverOffset = [180, -88];
    }

    //Predicate argument is: {color: color, xCoord: xCoord, yCoord: yCoord}
    _pixelMatcher({ xStart = 0, yStart = 0, xEnd = 1920, yEnd = 1080, xInc = 2, yInc = 2, pixelColorFunction = robot.getPixelColor.bind(robot), predicate = null }) {
        for (let x = Math.max(0, xStart); x < xEnd; x += xInc) {
            for (let y = Math.max(yStart); y < yEnd; y += yInc) {
                if (predicate === null) {
                    throw "You shall define a predicate when searching for pixel matching";
                }
                const candidate = { color: pixelColorFunction(x, y), xCoord: x, yCoord: y };
                if (predicate(candidate)) {
                    return candidate;
                }
            }
        }
        return null;
    }

    _currentDinoColor() {
        //dino color is the same of the game's baseline
        const xOffset = 1;
        const img = robot.screen.capture(
            this.startingPoint.xCoord - xOffset,
            this.startingPoint.yCoord,
            20,
            1);
        return this._pixelMatcher({
            xStart: xOffset,
            xEnd: img.width,
            yEnd: img.height,
            xInc: 1,
            yInc: 1,
            predicate: ({ color, xCoord, yCoord }) => {
                return color !== img.colorAt(0, 0);
            },
            pixelColorFunction: (x, y) => img.colorAt(x, y)
        }).color;
    }

    get startingPoint() {
        const [xInc, yInc] = [12, 9];
        const dinoLocation = this._pixelMatcher({
            xEnd: this.screenSize.width,
            yEnd: this.screenSize.height,
            xInc: xInc, //longest consecutives x-axis dino pixels
            yInc: yInc, //longest consecutives y-axis dino pixels
            predicate: ({ color, xCoord, yCoord }) => {
                return [this.dinoColorv1, this.dinoColorv2].includes(color) && robot.getPixelColor(xCoord - xInc, yCoord) === this.defaultBg;
            }
        });

        if (dinoLocation === null) {
            throw "Game not found";
        }

        //70px and 45px are just number bigger than dino width and height
        const startingPoint = this._pixelMatcher({
            xStart: dinoLocation.xCoord - 70,
            yStart: dinoLocation.yCoord - 45,
            xEnd: dinoLocation.xCoord + 1,
            yEnd: Math.min(dinoLocation.yCoord + 45, this.screenSize.height),
            xInc: 1,
            yInc: 1,
            predicate: ({ color, xCoord, yCoord }) => {
                return [this.dinoColorv1, this.dinoColorv2].includes(color);
            }
        });

        if( startingPoint === null) {
            throw "I found Dino but not startingPoint";
        }

        //Calculated so we can shadow the getter
        Object.defineProperty(this, "startingPoint", { value: startingPoint, writable: false, configurable: true });
        return this.startingPoint;
    }

    isGameOver() {
        const dinoColor = this._currentDinoColor();
        const [xOffset, yOffset] = this.gameoverOffset;
        const img = robot.screen.capture(
            this.startingPoint.xCoord + xOffset,
            this.startingPoint.yCoord + yOffset,
            100,//half of gameover text length
            1);
        const gameOverPixel = this._pixelMatcher({
            xEnd: img.width,
            yEnd: img.height,
            xInc: 1,
            yInc: 1,
            predicate: ({ color, xCoord, yCoord }) => {
                return color === dinoColor;
            },
            pixelColorFunction: (x, y) => img.colorAt(x, y)
        });
        return gameOverPixel !== null;
    }

    getNextObstacleDetails([xOffset = 100, yOffset = -54, width = 400, height = 40], yStep = 9) {
        const dinoColor = this._currentDinoColor();
        const biggestObstacle = 75;
        for (let i = Math.floor(height / yStep); i >= 0; --i) {
            const img = robot.screen.capture(
                this.startingPoint.xCoord + xOffset,
                this.startingPoint.yCoord + yOffset + yStep * i,
                width,
                1);

            const nextObstacleStart = this._pixelMatcher({
                xEnd: img.width,
                yEnd: img.height,
                xInc: 1,
                yInc: 1,
                predicate: ({ color, xCoord, yCoord }) => {
                    return color === dinoColor;
                },
                pixelColorFunction: (x, y) => img.colorAt(x, y)
            });

            if (nextObstacleStart === null) {
                continue;
            }

            const nextObstacleEnd = this._pixelMatcher({
                xStart: Math.min(nextObstacleStart.xCoord + biggestObstacle, img.width - 1),
                yStart: nextObstacleStart.yCoord,
                xEnd: img.width,
                yEnd: img.height,
                xInc: -1,
                yInc: 1,
                predicate: ({ color, xCoord, yCoord }) => {
                    return color === dinoColor;
                },
                pixelColorFunction: (x, y) => img.colorAt(x, y)
            });

            return {
                distance: nextObstacleStart.xCoord / width * 100,
                size: (nextObstacleEnd.xCoord - nextObstacleStart.xCoord) / biggestObstacle * 100,
                height: Math.abs(yOffset + yStep * i) / Math.abs(yOffset) * 100
            }
        }
        return {
            distance: 100,
            height: 0,
            size: 0
        };
    }
}

module.exports = SpriteLocator;