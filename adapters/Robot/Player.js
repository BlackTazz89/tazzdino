const robot = require('robotjs');

class Player {
    constructor(spriteLocator, logger, connector) {
        this.spriteLocator = spriteLocator;
        this.logger = logger;
        this.connector = connector;
        this.paused = false;
        this.releaseKey = 'up';
        this.pressKey = 'down';
        robot.setKeyboardDelay(1);
    }

    start({onStart, onGameOver}) {
        robot.moveMouse(this.spriteLocator.startingPoint.xCoord - 20, this.spriteLocator.startingPoint.yCoord);
        robot.mouseClick();
        this.logger.log('Starting new game');
        //waiting a bit to be sure that we focused the browser with mouseclick
        setTimeout(() => {
            robot.keyTap('space');
            onStart();
            this._gameOverLoop(onStart, onGameOver);
        }, 100);
    }

    _gameOverLoop(onStart, onGameOver) {
        this.checkGameOverId = setInterval(() => {
            if (!this.paused && this.spriteLocator.isGameOver()) {
                this.logger.log('Game over');
                onGameOver();
                this.reload();
                //waiting a bit to be sure browser has reloaded
                setTimeout(() => {
                    onStart();
                    robot.keyTap('space');
                }, 200);
            }
        }, 300);
    }

    performAction(key, action) {
        robot.keyToggle(key, action)
    }

    reload() {
        this.logger.log('Reloading page');
        let modifier = 'control';
        if (/^darwin/.test(process.platform)) {
            modifier = 'command';
        }
        robot.keyTap('r', modifier);
    }

    reset() {
        robot.keyToggle('up', this.releaseKey);
        robot.keyToggle('down', this.releaseKey);
    }

    pause() {
        this.paused = !this.paused;
        if (!this.paused) {
            robot.moveMouse(this.spriteLocator.startingPoint.xCoord - 20, this.spriteLocator.startingPoint.yCoord);
            robot.mouseClick();
        } else {
            this.reset();
        }
    }

    stop() {
        clearInterval(this.checkGameOverId);
    }
}

module.exports = Player;