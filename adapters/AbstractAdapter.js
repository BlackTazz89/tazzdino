// this class is kind of useless but for documentation and clarity of mind
class AbstractAdapter {
    constructor(logger) {
        this.logger = logger;
        this.observers = [];
    }

    registerObserver(observer) {
        this.observers.push(observer);
    }

    notifyObservers(data) {
        this.observers.forEach(observer => observer.notify(data));
    }

    start() { };
    stop() { };

    send_start() { };
    send_togglepause() { };
    send_keydown(key) { };
    send_keyup(key) { };
    send_restart() { };
}

module.exports = AbstractAdapter;