const AbstractAdapter = require("./AbstractAdapter");
const CustomServer = require("./CustomServer/CustomServer");

const keymap = {
    'up': 38,
    'down': 40
}

class CustomServerAdapter extends AbstractAdapter {
    constructor(logger) {
        super(logger);
        this.customServer = new CustomServer(logger, this);
    }

    start() {
        this.customServer.start();
    }

    stop() {
        this.customServer.stop();
    }

    send_start() {
        this.customServer.sendEvent({ type: "start" });
    }

    send_togglepause() {
        this.customServer.sendEvent({ type: "togglepause" });
    }

    send_keydown(key) {
        this.customServer.sendEvent({ type: "keydown", key: keymap[key] });
    }

    send_keyup(key) {
        this.customServer.sendEvent({ type: "keyup", key: keymap[key] });
    }

    send_restart() {
        this.customServer.sendEvent({ type: "restart" });
    }
}

module.exports = CustomServerAdapter;