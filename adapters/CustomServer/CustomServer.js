const serve = require('serve');
const WebSocket = require('ws');
const puppeteer = require('puppeteer');

class CustomServer {
    constructor(logger, connector) {
        this.logger = logger;
        this.connector = connector;
        this.observers = [];
        this.ws = undefined;
        this.server = undefined;
        this.wss = undefined;
    }

    start() {
        // static server
        this.server = serve('game/', {
            port: 8080,
            local: true,
            silent: true
        });

        // websocket server
        this.wss = new WebSocket.Server({
            port: 8081,
            clientTracking: true
        });

        this.wss.on('connection', (ws) => {
            if (!this.ws) {
                this.logger.log("Connection established");
                this.ws = ws;

                this.ws.on('message', message => this.connector.notifyObservers(JSON.parse(message)));
                this.ws.on('close', () => {
                    this.logger.log("Connection to websocket lost");
                    this.ws = undefined;
                });
            } else {
                this.logger.log("Connection request rejected");
                ws.close();
            }
        });

        this.logger.log("Starting headless chrome");

        setTimeout(async () => {
            const browser = await puppeteer.launch();
            const page = await browser.newPage();
            await page.goto('http://localhost:8080');
        }, 500);
    }

    sendEvent({ type, key = 0 }) {
        if (typeof this.ws !== "undefined") {
            this.ws.send(JSON.stringify({
                type: type,
                key: key
            }));
        } else {
            this.logger.log("WebSocket connection not available");
        }
    }

    stop() {
        this.server.stop();
    }
}

module.exports = CustomServer;
