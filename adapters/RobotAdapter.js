const AbstractAdapter = require('./AbstractAdapter');
const SpriteLocator = require('./Robot/SpriteLocator');
const Sensor = require('./Robot/Sensor');
const Player = require('./Robot/Player');

const [sensorX, sensorY, sensorWidth, sensorHeight] = [100, -54, 400, 40];

class RobotAdapter extends AbstractAdapter {
    constructor(logger) {
        super(logger);
        this.interval = 25; //ms

        this.spriteLocator = new SpriteLocator();
        this.sensor = new Sensor([sensorX, sensorY, sensorWidth, sensorHeight], this.spriteLocator, this.logger, this);
        this.player = new Player(this.spriteLocator, this.logger, this);
    }

    start() {
        this.player.start({
            onStart: () => this.notifyObservers({
                flags: {
                    playing: false,
                    paused: this.paused,
                    crashed: false
                }
            }),
            onGameOver: () => this.notifyObservers({
                flags: {
                    playing: false,
                    paused: this.paused,
                    crashed: true
                }
            })
        });
    };

    stop() {
        this.sensor.stop();
    };

    send_start() {
        this.sensor.reset();
        this.sensor.activate(this.interval);
    };

    send_togglepause() {
        this.player.pause();
        this.sensor.pause();
    };

    send_keydown(key) { 
        this.player.performAction(key, 'down');
    };
    send_keyup(key) { 
        this.player.performAction(key, 'up');
    };

    send_restart() {
        this.sensor.stop();
    };
}

module.exports = RobotAdapter;