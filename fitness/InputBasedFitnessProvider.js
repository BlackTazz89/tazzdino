const AbstractFitnessProvider = require('./AbstractFitnessProvider');

class InputBasedFitnessProvider extends AbstractFitnessProvider {
    constructor(logger) {
        super();
        this.logger = logger;
        this.previousInputs = [];
        this.previousOutputs = [];
        this.fitnessGeneratorInstance = this.fitnessGenerator();
    }

    _getFitness(previousInputs, currentInputs, previousOutputs, outputs) {
        const [prevDistance, prevSize, prevHeight, prevSpeed] = previousInputs;
        if (currentInputs === null) {
            return prevDistance > 0 ? -1 : 0;
        }
        const [currDistance, currSize, currHeight, currSpeed] = currentInputs;
        return (prevDistance - currDistance) < 0 && currDistance > prevSize && prevSpeed > 0 ? 1 : 0;
    }

    *fitnessGenerator() {
        this.fitness = 0;
        let inputs, outputs;
        while({inputs, outputs} = yield this.fitness) {
            this.fitness += this._getFitness(this.previousInputs, inputs, this.previousOutputs, outputs);
            this.previousInputs = inputs;
            this.previousOutputs = outputs;
        }
    }

    reset() {
        this.fitnessGeneratorInstance = this.fitnessGenerator();
        this.logger.log("Reset fitness generator");
    }

    provide(fitnessMetadata, {up, down}) {
        return this.fitnessGeneratorInstance.next({
            inputs: [fitnessMetadata.distance, fitnessMetadata.width, fitnessMetadata.height, fitnessMetadata.speed],
            output: [up, down]
        }).value;
    }
}

module.exports = InputBasedFitnessProvider;