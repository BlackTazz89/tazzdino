class AbstractFitnessProvider {
    constructor() { }

    provide(fitnessMetadata, outputs) { }

    reset() { }
}

module.exports = AbstractFitnessProvider;