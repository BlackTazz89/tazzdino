const AbstractFitnessProvider = require('./AbstractFitnessProvider');

class DistanceRanFitnessProvider extends AbstractFitnessProvider {
    provide(fitnessMetadata, outputs) { 
        return fitnessMetadata.distanceRan;
    }
}

module.exports = DistanceRanFitnessProvider;