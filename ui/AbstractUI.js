const fs = require('fs');
const path = require('path');

class AbstractUI {
    constructor(screenTitle, saveDir, autosave = true) {
        this.screenTitle = screenTitle;
        this.autosave = autosave;
        this.saveDir = saveDir;

        this.paused = false;
        this.logstream = fs.createWriteStream("log.txt");

        if (this.autosave) {
            this._createSaveDirIfMissing();
        }
    }

    _createSaveDirIfMissing() {
        const saveDirPath = path.join(__dirname, '..', this.saveDir);
        if (!fs.existsSync(saveDirPath)) {
            fs.mkdirSync(saveDirPath);
        }
    }

    updateSensors({ distance, speed, height, width, types: { small, large, pterodactyl }, action: [up, down] }) { }

    updateInfo(text) { }

    updateFitnessGraph(maxFitnessValue, meanFitnessValue) { }

    log(text) {
        this.logstream.write(`${new Date().toISOString()}: ${text}\n`);
    }

    onKeyPress(key, callback) { }

    onMouseClick(button, callback) { }

    saveData() {
        if (this.dataSupplier) {
            this.log('Saving...');
            const fname = `data_${new Date().toISOString()}.json`;
            fs.writeFileSync(path.join(__dirname, '..', this.saveDir, fname), this.dataSupplier());
            this.log(`Saved '${fname}' successfully`);
        }
    }

    onSave(dataSupplier) {
        this.dataSupplier = dataSupplier;
    }

    onExit(callback) { }

    onDataLoad(callback) { }

    stop() {
        if (this.autosave) {
            this.saveData();
        }
        this.log("Shutting down");
    }

    pause() {
        this.paused = !this.paused;
    }

    reset() { }

    render() { }
}

module.exports = AbstractUI;