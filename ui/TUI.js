const blessed = require('blessed');
const contrib = require('blessed-contrib');
const path = require('path');
const fs = require('fs');
const _ = require('lodash');
const AbstractUI = require('./AbstractUI');

class TUI extends AbstractUI {
    constructor(screenTitle, saveDir, autosave = true) {
        super(screenTitle, saveDir, autosave);
        this.maxFitnessValues = [];
        this.meanFitnessValues = [];

        this.screen = blessed.screen({
            smartCSR: true,
            title: screenTitle
        });

        this.grid = new contrib.grid({
            rows: 4,
            cols: 12,
            screen: this.screen
        });

        this.sensors = this.grid.set(0, 0, 1, 12, contrib.bar, {
            label: 'Sensors',
            barWidth: 6,
            barSpacing: 6,
            xOffset: 0,
            maxHeight: 100
        });

        this.logs = this.grid.set(1, 0, 1, 8, blessed.log, {
            fg: 'green',
            selectedFg: 'green',
            label: 'Log events',
            keys: true,
            vi: true,
            mouse: true,
            alwaysScroll: true,
            scrollable: true,
            scrollbar: {
                style: {
                    bg: 'green'
                }
            }
        });
        this.logs.on('click', () => this.logs.focus());

        this.genomeInfo = this.grid.set(1, 8, 1, 4, blessed.Text, {
            label: 'Current genome',
            fg: 'green',
            align: 'center'
        });

        this.fitnessGraph = this.grid.set(2, 0, 1, 12, contrib.line, {
            style: {
                line: "yellow",
                text: "green",
                baseline: "black"
            },
            xLabelPadding: 3,
            xPadding: 5,
            showLegend: true,
            label: 'Fitness'
        });

        this.tree = this.grid.set(3, 0, 1, 6, contrib.tree, {
            label: 'Saved Genomes',
            fg: 'green'
        });
        this.tree.on('click', () => this.tree.focus());
        this.screen.key(['r'], () => this.refreshTree());

        this.save = this.grid.set(3, 6, 1, 6, blessed.box, {
            label: 'Save genomes',
            align: 'center',
            valign: 'middle',
            content: 'Save',
            border: {
                type: 'line'
            },
            style: {
                fg: 'white',
                bg: 'magenta',
                border: {
                    fg: '#f0f0f0'
                },
                hover: {
                    bg: 'green'
                }
            }
        });

        this.refreshTree();
    }

    refreshTree() {
        const saveDirPath = path.join(__dirname, '..', this.saveDir);
        const ext = '.json';
        const children = fs.readdirSync(saveDirPath)
            .filter(file => path.extname(file).toLowerCase() === ext)
            .reduce((acc, file) => {
                acc[file] = {};
                return acc;
            }, {});
        this.tree.setData({
            name: 'Genomes',
            extended: true,
            children: children
        });
    }

    updateSensors({ distance, speed, height, width, types: { small, large, pterodactyl }, action: [up, down] }) {
        this.sensors.setData({
            titles: ['Distance', 'Speed', 'Height', 'Width', 'T_SMALL', 'T_LARGE', 'T_PT', 'Up', 'Down'],
            data: [distance, speed, height, width, small, large, pterodactyl, up, down].map(Math.round)
        });
    }

    updateInfo(text) {
        this.genomeInfo.setText(text);
    }

    updateFitnessGraph(maxFitnessValue, meanFitnessValue) {
        this.maxFitnessValues.push(maxFitnessValue);
        this.meanFitnessValues.push(meanFitnessValue);
        this.fitnessGraph.setData([{
            title: 'Max',
            x: _.range(0, this.maxFitnessValues.length),
            y: this.maxFitnessValues,
            style: {
                line: 'red'
            }
        }, {
            title: 'Mean',
            x: _.range(0, this.meanFitnessValues.length),
            y: this.meanFitnessValues,
            style: {
                line: 'yellow'
            }
        }]);
    }

    log(text) {
        super.log(text);
        this.logs.log(text);
    }

    onKeyPress(key, callback) {
        this.screen.key([key], callback);
    }

    onMouseClick(button, callback) {
        this.screen.on('mouse', (event) => {
            if (event.action === 'mousedown' && event.button === 'left' && !this.paused) {
                callback();
            }
        });
    }

    saveData() {
        super.saveData();
        this.refreshTree();
    }

    onSave(dataSupplier) {
        super.onSave(dataSupplier);
        this.save.on('click', () => this.saveData());
    }

    onExit(callback) {
        this.onKeyPress('C-c', callback);
    }

    onDataLoad(callback) {
        this.tree.on('select', (node) => {
            const ext = '.json';
            if (path.extname(node.name).toLowerCase() === ext) {
                const loadedData = JSON.parse(fs.readFileSync(path.join(__dirname, '..', this.saveDir, node.name)));
                callback(loadedData);
            }
            this.refreshTree();
        });
    }

    stop() {
        super.stop();
        this.screen.destroy();
        this.logs = console;
    }

    reset() {
        this.sensors.setData({
            titles: [],
            data: []
        });
        this.genomeInfo.setText('');
    }

    render() {
        this.screen.render();
    }
}

module.exports = TUI;