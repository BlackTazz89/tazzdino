const AbstractUI = require('./AbstractUI');

class ConsoleUI extends AbstractUI {
    constructor(screenTitle, saveDir, autosave = true) {
        super(screenTitle, saveDir, autosave);
    }

    updateFitnessGraph(maxFitnessValue, meanFitnessValue) { }

    log(text) {
        super.log(text);
        console.log(text);
    }

    onExit(callback) {
        process.on('SIGINT', callback);
        process.on('SIGTERM', callback);
    }

    updateFitnessGraph(maxFitnessValue, meanFitnessValue) { 
        this.log(`Avg Fitness:\t${meanFitnessValue}`);
        this.log(`Max Fitness:\t${maxFitnessValue}`);
    }

}

module.exports = ConsoleUI;