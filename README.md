# Requirements
- `nodejs` (tested with 9.11.1 but should work wth anything 8+)
- `yarn`
- a terminal with ncurses support (basically anything these days)

## Linux
- `libx11-dev`
- `libxtst-dev`
- `libpng-dev`
- `liblz-dev`

## Mac OSX
Install XCode and then run:
```
~$ xcode-select --install
```

# Install
```
git clone git@gitlab.com:BlackTazz89/tazzdino.git
cd tazzdino
yarn install
```

# Run
You need to have a tab with the game visible on the screen at all times.
Run with:
```
node index.js
```