const _ = require('lodash');

class Controller {
    constructor({ ui, neuralEngine, adapter, fitnessProvider }) {
        this.ui = ui;
        this.neuralEngine = neuralEngine;
        this.adapter = adapter;
        this.fitnessProvider = fitnessProvider;
        this.types = ['CACTUS_SMALL', 'CACTUS_LARGE', 'PTERODACTYL'];

        // init code
        const onPause = () => {
            this.adapter.send_togglepause();
            this.ui.log("Toggling pause");
            this.ui.pause();
        };

        const dataSupplier = () => JSON.stringify(this.neuralEngine.getStatus().genomes.map(genome => genome.serialize()));

        const onDataLoad = (data) => {
            this.neuralEngine = new NeuralEngine(data.map(Genome.fromJSON), newGenerationStratety, logger);
            this.neuralEngine.start();
            this.adapter.send_restart();
            this.ui.reset();
            this.ui.log('Genome\'s data loaded from file');
        };

        const onExit = () => {
            this.ui.stop();
            this.adapter.stop();
            setTimeout(() => process.exit(0), 500); // allow things to end gracefully
        };

        this.ui.onKeyPress('p', onPause);
        this.ui.onMouseClick('left', onPause);
        this.ui.onSave(dataSupplier);
        this.ui.onDataLoad(onDataLoad);
        this.ui.onExit(onExit);
        this.ui.render();

        this.adapter.registerObserver(this);
    }

    start() {
        this.neuralEngine.start();
        this.adapter.start();
        this.ui.render();
    }

    _bool2number(bool) {
        if (bool)
            return 100;
        return 0;
    }

    notify(data) {
        if (!data.flags.playing && !data.flags.paused && !data.flags.crashed) {
            this.ui.render();
            this.adapter.send_start();
        } else if (data.flags.playing && !data.flags.crashed) {
            let inputs = [100, 0, 0, 0, 0, 0, 0];
            if (typeof data.closest !== "undefined" && Object.keys(data.closest).length !== 0) {
                const typearray = this.types.map((type) => this._bool2number(data.closest.type === type));
                inputs = [data.closest.distance, data.closest.speed, data.closest.height, data.closest.width].concat(typearray);
            }
            const [up, down] = this.neuralEngine.inputs(inputs).map(x => x * 100);
            if (up > 50) {
                this.adapter.send_keydown('up');
            } else {
                this.adapter.send_keyup('up');
            }
            if (down > 50) {
                this.adapter.send_keydown('down');
            } else {
                this.adapter.send_keyup('down');
            }
            this.ui.updateSensors({
                distance: inputs[0],
                speed: inputs[1],
                height: inputs[2],
                width: inputs[3],
                types: {
                    small: inputs[4],
                    large: inputs[5],
                    pterodactyl: inputs[6]
                },
                action: [up, down]
            });

            this.neuralEngine.updateFitness(this.fitnessProvider.provide(data.fitnessMetadata, [up, down]));
            const { currentIdx, currentGenome } = this.neuralEngine.getStatus();
            this.ui.updateInfo(`Genome n°: ${currentIdx + 1}\nGeneration: ${currentGenome.generation}\nFitness: ${currentGenome.fitness.toFixed(3)}\n`);
            this.ui.render();
        } else if (data.flags.crashed) {
            const status = this.neuralEngine.getStatus();
            if (status.currentIdx === status.total - 1) {
                this.ui.updateFitnessGraph(_.maxBy(status.genomes, 'fitness').fitness, _.meanBy(status.genomes, 'fitness'))
            }
            this.neuralEngine.end();
            this.ui.reset();
            this.fitnessProvider.reset();
            this.adapter.send_restart();
        }
    }
}

module.exports = Controller;