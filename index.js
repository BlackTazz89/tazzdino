const path = require('path');
const fs = require('fs');
const _ = require('lodash');
const Yargs = require('yargs')

const get_pluggables = (directory) => fs.readdirSync(directory)
    .filter(path => fs.statSync(`./${directory}/${path}`).isFile())
    .filter(path => path.endsWith('.js'))
    .filter(path => !path.toLowerCase().includes('abstract'))
    .map(path => path.substring(0, path.length - 3));

const parsedArgs = Yargs
    .option('l', {
        alias: 'hiddenlayers',
        default: [4],
        describe: 'An array (space separated) used to define the underlying MLP',
        type: 'array'
    })
    .option('m', {
        alias: 'mutationrate',
        default: 0.05,
        describe: 'Probability that a single gene will mutate',
        type: 'number'
    })
    .option('a', {
        alias: 'adapter',
        describe: 'Used to select the method for controlling Dino',
        demandOption: true,
        choices: get_pluggables('adapters')
    })
    .option('f', {
        alias: 'fitness',
        describe: 'Used to select the method for calculating the fitness',
        demandOption: true,
        choices: get_pluggables('fitness')
    })
    .option('p', {
        alias: 'population',
        default: 10,
        describe: 'How many individuals per generation',
        type: 'number'
    })
    .option('s', {
        alias: 'autosave',
        default: true,
        describe: 'Automatically saves population upon exit',
        type: 'boolean'
    })
    .option('t', {
        alias: 'neuralstrategy',
        describe: 'Choose the neural strategy for mutation and crossover',
        demandOption: true,
        choices: get_pluggables('neuralNetwork/strategies')
    })
    .option('u', {
        alias: 'tui',
        describe: 'Use a Text [based] User Interface',
        default: true,
        type: 'boolean'
    })
    .version('0.9')
    .help()
    .usage('Train a MLP to win against google chrome downasaur!')
    .epilogue('For more info visit https://gitlab.com/BlackTazz89/tazzdino')
    .wrap(Yargs.terminalWidth())
    .argv;

const UI = parsedArgs.tui ? require('./ui/TUI') : require('./ui/ConsoleUI');
const Genome = require('./neuralNetwork/Genome');
const NeuralEngine = require('./neuralNetwork/NeuralEngine');
const Controller = require('./Controller');
const FitnessProvider = require(`./fitness/${parsedArgs.fitness}`)
const Adapter = require(`./adapters/${parsedArgs.adapter}`)
const newGenerationStratety = require(`./neuralNetwork/strategies/${parsedArgs.neuralstrategy}`);

const ui = new UI('Dashboard', 'saves', parsedArgs.autosave);
const logger = ui;
const genomes = Array.from({ length: parsedArgs.population }, () => new Genome(7, parsedArgs.hiddenlayers, 2, parsedArgs.mutationrate));
const neuralEngine = new NeuralEngine(genomes, newGenerationStratety, logger);
const fitnessProvider = new FitnessProvider(logger);
const adapter = new Adapter(logger);

logger.log(`Param decoded:`);
logger.log(`\t Population:     ${parsedArgs.population}`);
logger.log(`\t Hiddelayers:    ${parsedArgs.hiddenlayers}`);
logger.log(`\t MutationRate:   ${parsedArgs.mutationrate}`);
logger.log(`\t Fitness:        ${parsedArgs.fitness}`);
logger.log(`\t Connector:      ${parsedArgs.adapter}`);
logger.log(`\t Autosave:       ${parsedArgs.autosave}`)
logger.log(`\t NeuralStrategy: ${parsedArgs.neuralstrategy}`)
logger.log('');

const controller = new Controller({ ui, neuralEngine, adapter, fitnessProvider });

controller.start();